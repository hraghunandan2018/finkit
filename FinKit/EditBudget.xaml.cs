﻿using System;
using System.Collections.Generic;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace FinKit
{
    public partial class EditBudget : ContentPage
    {
        public EditBudget()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            budgetValue.Text = Preferences.Get("BudgetValue", "0");
            entertainmentValue.Text = Preferences.Get("EntertainmentValue", "0");
            transportationValue.Text = Preferences.Get("TransportationValue", "0");
            shopsValue.Text = Preferences.Get("ShopsValue", "0");
            clothesValue.Text = Preferences.Get("ClothesValue", "0");
            travelValue.Text = Preferences.Get("TravelValue", "0");
            foodValue.Text = Preferences.Get("FoodValue", "0");
            housingValue.Text = Preferences.Get("HousingValue", "0");
            insuranceValue.Text = Preferences.Get("InsuranceValue", "0");
            groceryValue.Text = Preferences.Get("GroceryValue", "0");

        }


        public void editBudget (object sender, EventArgs e)
        {
            budgetValue.IsVisible = false;
            editBudgetIcon.IsVisible = false;
            enterBudgetValue.IsVisible = true;
            saveBudgetIcon.IsVisible = true;
            enterBudgetValue.Placeholder = Preferences.Get("BudgetValue", "0");

        }

        public void saveBudget (object sender, EventArgs e)
        {
            enterBudgetValue.IsVisible = false;
            saveBudgetIcon.IsVisible = false;
            editBudgetIcon.IsVisible = true;

            Preferences.Set("BudgetValue", enterBudgetValue.Text.ToString());

            budgetValue.Text = Preferences.Get("BudgetValue", "0");

            budgetValue.IsVisible = true;
        }

        public void editEntertainment(object sender, EventArgs e)
        {
            entertainmentValue.IsVisible = false;
            editEntertainmentIcon.IsVisible = false;
            enterEntertainmentValue.IsVisible = true;
            saveEntertainmentIcon.IsVisible = true;

        }

        public void saveEntertainment(object sender, EventArgs e)
        {
            enterEntertainmentValue.IsVisible = false;
            saveEntertainmentIcon.IsVisible = false;
            editEntertainmentIcon.IsVisible = true;
            entertainmentValue.IsVisible = true;

            Preferences.Set("EntertainmentValue", enterEntertainmentValue.Text.ToString());

            entertainmentValue.Text = Preferences.Get("EntertainmentValue", "0");

            entertainmentValue.IsVisible = true;
        }

        public void editShops(object sender, EventArgs e)
        {
            shopsValue.IsVisible = false;
            editShopsIcon.IsVisible = false;
            enterShopsValue.IsVisible = true;
            saveShopsIcon.IsVisible = true;
        }

        public void saveShops(object sender, EventArgs e)
        {
            enterShopsValue.IsVisible = false;
            saveShopsIcon.IsVisible = false;
            editShopsIcon.IsVisible = true;
            shopsValue.IsVisible = true;

            Preferences.Set("ShopsValue", enterShopsValue.Text.ToString());

            shopsValue.Text = Preferences.Get("ShopsValue", "0");

            shopsValue.IsVisible = true;
        }

        public void editInsurance(object sender, EventArgs e)
        {
            insuranceValue.IsVisible = false;
            editInsuranceIcon.IsVisible = false;
            enterInsuranceValue.IsVisible = true;
            saveInsuranceIcon.IsVisible = true;

        }

        public void saveInsurance(object sender, EventArgs e)
        {
            enterInsuranceValue.IsVisible = false;
            saveInsuranceIcon.IsVisible = false;
            editInsuranceIcon.IsVisible = true;
            insuranceValue.IsVisible = true;

            Preferences.Set("InsuranceValue", enterInsuranceValue.Text.ToString());

            insuranceValue.Text = Preferences.Get("InsuranceValue", "0");

            insuranceValue.IsVisible = true;
        }

        public void editHousing(object sender, EventArgs e)
        {
            housingValue.IsVisible = false;
            editHousingIcon.IsVisible = false;
            enterHousingValue.IsVisible = true;
            saveHousingIcon.IsVisible = true;

        }

        public void saveHousing(object sender, EventArgs e)
        {
            enterHousingValue.IsVisible = false;
            saveHousingIcon.IsVisible = false;
            editHousingIcon.IsVisible = true;
            housingValue.IsVisible = true;

            Preferences.Set("HousingValue", enterHousingValue.Text.ToString());

            housingValue.Text = Preferences.Get("HousingValue", "0");

            housingValue.IsVisible = true;
        }

        public void editTravel(object sender, EventArgs e)
        {
            travelValue.IsVisible = false;
            editTravelIcon.IsVisible = false;
            enterTravelValue.IsVisible = true;
            saveTravelIcon.IsVisible = true;

        }

        public void saveTravel(object sender, EventArgs e)
        {
            enterTravelValue.IsVisible = false;
            saveTravelIcon.IsVisible = false;
            editTravelIcon.IsVisible = true;
            travelValue.IsVisible = true;

            Preferences.Set("TravelValue", enterTravelValue.Text.ToString());

            travelValue.Text = Preferences.Get("TravelValue", "0");

            travelValue.IsVisible = true;
        }

        public void editTransportation(object sender, EventArgs e)
        {
            transportationValue.IsVisible = false;
            editTransportationIcon.IsVisible = false;
            enterTransportationValue.IsVisible = true;
            saveTransportationIcon.IsVisible = true;

        }

        public void saveTransportation(object sender, EventArgs e)
        {
            enterTransportationValue.IsVisible = false;
            saveTransportationIcon.IsVisible = false;
            editTransportationIcon.IsVisible = true;
            transportationValue.IsVisible = true;

            Preferences.Set("TransportationValue", enterTransportationValue.Text.ToString());

            transportationValue.Text = Preferences.Get("TransportationValue", "0");

            transportationValue.IsVisible = true;
        }

        public void editFood(object sender, EventArgs e)
        {
            foodValue.IsVisible = false;
            editFoodIcon.IsVisible = false;
            enterFoodValue.IsVisible = true;
            saveFoodIcon.IsVisible = true;

        }

        public void saveFood(object sender, EventArgs e)
        {
            enterFoodValue.IsVisible = false;
            saveFoodIcon.IsVisible = false;
            editFoodIcon.IsVisible = true;
            foodValue.IsVisible = true;

            Preferences.Set("FoodValue", enterFoodValue.Text.ToString());

            foodValue.Text = Preferences.Get("FoodValue", "0");

            foodValue.IsVisible = true;
        }

        public void editClothes(object sender, EventArgs e)
        {
            clothesValue.IsVisible = false;
            editClothesIcon.IsVisible = false;
            enterClothesValue.IsVisible = true;
            saveClothesIcon.IsVisible = true;

        }

        public void saveClothes(object sender, EventArgs e)
        {
            enterClothesValue.IsVisible = false;
            saveClothesIcon.IsVisible = false;
            editClothesIcon.IsVisible = true;
            clothesValue.IsVisible = true;

            Preferences.Set("ClothesValue", enterClothesValue.Text.ToString());

            clothesValue.Text = Preferences.Get("ClothesValue", "0");

            clothesValue.IsVisible = true;
        }

        public void editGrocery(object sender, EventArgs e)
        {
            groceryValue.IsVisible = false;
            editGroceryIcon.IsVisible = false;
            enterGroceryValue.IsVisible = true;
            saveGroceryIcon.IsVisible = true;

        }

        public void saveGrocery(object sender, EventArgs e)
        {
            enterGroceryValue.IsVisible = false;
            saveGroceryIcon.IsVisible = false;
            editGroceryIcon.IsVisible = true;
            groceryValue.IsVisible = true;

            Preferences.Set("GroceryValue", enterGroceryValue.Text.ToString());

            groceryValue.Text = Preferences.Get("GroceryValue", "0");

            groceryValue.IsVisible = true;
        }

        void editInsuranceIcon_Clicked(System.Object sender, System.EventArgs e)
        {
        }
    }
}
