﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FinKit
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MyTabbedPage : TabbedPage
    {
        public MyTabbedPage()
        {
            InitializeComponent();

            Children.Add(new Dashboard());
            Children.Add(new Transactions());
            Children.Add(new EditBudget());

            BarBackgroundColor = Color.FromHex("#008080");
            BarTextColor = Color.White;
            SelectedTabColor = Color.White;
            UnselectedTabColor = Color.Black;
        }
    }
}
