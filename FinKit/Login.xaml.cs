﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using Newtonsoft.Json;
using Plugin.Fingerprint;
using Xamarin.Forms;
using static FinKit.App;

namespace FinKit
{
    public partial class Login : ContentPage
    {
        public Login()
        {
            InitializeComponent();
        }

        public async void SignIn(System.Object sender, System.EventArgs e)
        {

            //var client = new HttpClient();

            //User user = new User()
            //{
            //    Email = email.Text,
            //    Password = password.Text,
            //    ActionType = "RetrieveUser"
            //};

            //var jsonString = JsonConvert.SerializeObject(user);
            //var requestContent = new StringContent(jsonString);
            //var result = await client.PostAsync(APIEndPoints.Login, requestContent);
            //var resultContent = await result.Content.ReadAsStringAsync();
            //var response = JsonConvert.DeserializeObject<User>(resultContent);

            //ProcessResponse(response);

            Navigation.PushAsync(new MyTabbedPage());

        }

        public void ProcessResponse(User user)
        {
            if(user.Name == "Not Found")
            {
                DisplayAlert("Error", "Your account was not found, please try again.", "Dismiss");
            }
            else
            {
                DisplayAlert("FinKit", "Welcome to FinKit, " + user.Name + "!", "Dismiss");
                Navigation.PushAsync(new MyTabbedPage());
            }

        }


        async void FaceID(System.Object sender, System.EventArgs e)
        {
            var availability = await CrossFingerprint.Current.IsAvailableAsync();

            if (!availability)
            {
                await DisplayAlert("Warning!", "Authentication not supported", "OK");
                return;
            }

            var authResult = await CrossFingerprint.Current.AuthenticateAsync(new Plugin.Fingerprint.Abstractions.AuthenticationRequestConfiguration
                ("Head's Up!", "ResKit would like to access Touch ID"));

            if (authResult.Authenticated)
            {
                await Navigation.PushAsync(new MyTabbedPage());
                Navigation.RemovePage(this);
            }

        }


    }
}
