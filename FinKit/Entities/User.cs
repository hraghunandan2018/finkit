﻿using System;
namespace FinKit
{
    public class User
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string ActionType { get; set; }
        public string Name { get; set; }
    }
}
