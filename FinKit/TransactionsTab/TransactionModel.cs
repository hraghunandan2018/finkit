﻿using System;
namespace FinKit
{
    public class TransactionModel
    {
        public string Amount { get; set; }
        public string Date { get; set; }
        public string Category { get; set; }
        public string Vendor { get; set; }
        public string ActionType { get; set; }
    }
}
