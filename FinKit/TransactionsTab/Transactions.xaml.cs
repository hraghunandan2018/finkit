﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Essentials;
using Xamarin.Forms.Xaml;
using static FinKit.App;
using Newtonsoft.Json;
using System.Net.Http;
using System.Globalization;


namespace FinKit
{
    public partial class Transactions : ContentPage
    {
        public Transactions()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            string fileName = "transactionDB.db3";

            string documentPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            string path = Path.Combine(documentPath, fileName);

            using (SQLite.SQLiteConnection conn = new SQLite.SQLiteConnection(path))
            {
                conn.CreateTable<TransactionModel>();
                var feedbacks = conn.Table<TransactionModel>().ToList();

                TransactionList.ItemsSource = feedbacks;

            }

        }

        private void AddTransaction(object sender, EventArgs e)
        {
            Navigation.PushAsync(new AddTransaction());
        }

        async public void DeleteTransactions(object sender, EventArgs e)
        {
            bool res = await DisplayAlert("Warning", "Are you sure you want to delete all transactions?", "Yes", "Cancel");

            if (res)
            {
                var file = Path.Combine(FileSystem.CacheDirectory, APIEndPoints.transactionsFile);

                File.WriteAllText(file, " ");

                string fileName = "transactionDB.db3";

                string documentPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
                string path = Path.Combine(documentPath, fileName);

                using (SQLite.SQLiteConnection conn = new SQLite.SQLiteConnection(path))
                {
                    conn.CreateTable<TransactionModel>();
                    conn.Execute("DELETE FROM TransactionModel");

                }

                _ = Navigation.PushAsync(new Transactions());
                _ = Navigation.PopAsync();

            }
        }


    }
}
