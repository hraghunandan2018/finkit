﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using Newtonsoft.Json;
using Xamarin.Essentials;
using Xamarin.Forms;
using static FinKit.App;

namespace FinKit
{
    public partial class AddTransaction : ContentPage
    {
        public AddTransaction()
        {
            InitializeComponent();
        }

        async void SubmitTransaction(object sender, EventArgs e)
        {
            SubmitButton.IsVisible = false;

            activityIndicator.IsEnabled = true;
            activityIndicator.IsRunning = true;
            activityIndicator.IsVisible = true;

            APIEndPoints.spent += Convert.ToDouble(amount.Text);
            Preferences.Set("SpentValue", APIEndPoints.spent);

            var client = new HttpClient();

            var model = new TransactionModel()
            {
                Amount = amount.Text,
                Date = date.Date.ToString("d"),
                Category = (string)category.SelectedItem,
                Vendor = vendor.Text,
                ActionType = "AddTransaction"
            };

            TransactionModel tranModel = new TransactionModel();

            tranModel.Amount = amount.Text;
            tranModel.Date = date.Date.ToString("d");
            tranModel.Category = (string)category.SelectedItem;
            tranModel.Vendor = vendor.Text;

            string fileName = "transactionDB.db3";

            string documentPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            string path = Path.Combine(documentPath, fileName);

            using (SQLite.SQLiteConnection conn = new SQLite.SQLiteConnection(path))
            {
                conn.CreateTable<TransactionModel>();
                conn.Insert(tranModel);
            }


            //store data in a text file that can be exported
            var file = Path.Combine(FileSystem.CacheDirectory, APIEndPoints.transactionsFile);

            string exportTransactions = JsonConvert.SerializeObject(model);

            using (StreamWriter sw = File.AppendText(file))
            {
                sw.WriteLine(exportTransactions + "\n");
            }


            //check for internet access
            var current = Connectivity.NetworkAccess;

            if (current == NetworkAccess.Internet)
            {
                var jsonString = JsonConvert.SerializeObject(model);
                var requestContent = new StringContent(jsonString);
                var result = await client.PostAsync(APIEndPoints.transactionsAPI, requestContent);
                var resultContent = await result.Content.ReadAsStringAsync();
                var response = JsonConvert.DeserializeObject<ResponseModel>(resultContent);
                

                activityIndicator.IsEnabled = false;
                activityIndicator.IsRunning = false;
                activityIndicator.IsRunning = false;

                ProcessResponse(response);

                _ = Navigation.PopAsync();

            }
            else
            {
                _ = DisplayAlert("Error", "Your device is currently not connected to the internet, but your CICO entry was saved to the ResKit App.", "Ok");
                _ = Navigation.PopAsync();

            }

        }

        private void ProcessResponse(ResponseModel responseModel)
        {

            if (responseModel.Vendor == "Success")
                DisplayAlert("Success", "Your entry was successful", "Ok");
            else
                DisplayAlert("Failure", "Your entry was unsuccessful", "Ok");
        }

    }
}
