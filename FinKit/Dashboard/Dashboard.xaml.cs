﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using Xamarin.Forms;
using Xamarin.Essentials;


namespace FinKit
{
    public partial class Dashboard : ContentPage
    {
        public Dashboard()
        {
            InitializeComponent();
            monthlyBudget.Text = "$" + Preferences.Get("BudgetValue", "0");
            amountSpent.Text = "$" + Preferences.Get("SpentValue", "0");
            ring.AnimatedProgress = Convert.ToDouble(Preferences.Get("SpentValue", "0")) / Convert.ToDouble(Preferences.Get("BudgetValue", "0"));
        }

        void logout(System.Object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new Login());
        }
    }
}