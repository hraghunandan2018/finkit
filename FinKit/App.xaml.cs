﻿using System;
using System.Collections.Generic;
using Com.OneSignal;
using Com.OneSignal.Abstractions;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;


[assembly: ExportFont("Oswald-Regular.ttf", Alias = "Oswald")]
[assembly: ExportFont("Monda-Regular.ttf", Alias = "Monda")]
[assembly: ExportFont("Poppins-Bold.ttf", Alias = "PoppinsBold")]
[assembly: ExportFont("Poppins-Regular.ttf", Alias = "Poppins")]
[assembly: ExportFont("Poppins-Italic.ttf", Alias = "PoppinsItalic")]


namespace FinKit
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();


            MainPage = new NavigationPage(new Login())
            {
                BarBackgroundColor = Color.White,
                BarTextColor = Color.FromHex("008080")
            };

            //Remove this method to stop OneSignal Debugging  
            OneSignal.Current.SetLogLevel(LOG_LEVEL.VERBOSE, LOG_LEVEL.NONE);

            OneSignal.Current.StartInit("ec4ddb24-8a96-435a-96ab-35516f414de3")
            .Settings(new Dictionary<string, bool>() {
    { IOSSettings.kOSSettingsKeyAutoPrompt, false },
    { IOSSettings.kOSSettingsKeyInAppLaunchURL, false } })
            .InFocusDisplaying(OSInFocusDisplayOption.Notification)
            .EndInit();

            // The promptForPushNotificationsWithUserResponse function will show the iOS push notification prompt. We recommend removing the following code and instead using an In-App Message to prompt for notification permission (See step 7)
            OneSignal.Current.RegisterForPushNotifications();


        }

        public static class APIEndPoints
        {
            public static double spent = 0.0;

            public static string transactionsFile = "transactions.txt";

            public static string transactionsAPI = "https://script.google.com/macros/s/AKfycbxvh1Ua0bCo-DLw_qtk2rMrFI2wkAaz0139fMR2TC-OFBbPjgt6waws77-7_nsLkSPv/exec";

            public static string Login = "https://script.google.com/macros/s/AKfycbwcdZDI-jvmupiUGfvtW4kXH1rRVdBX2b-2qGtHm9Kle9B0JxWjDub3JwlMpOxOsbQn/exec";
        }


        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
